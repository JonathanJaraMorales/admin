package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.Converter.GroupConverter;
import static com.jjmsoftsolutions.controller.UserController.getUsersByGroupAndFirstName;
import com.jjmsoftsolutions.datamodel.UserDataModel;
import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.entity.Role;
import com.jjmsoftsolutions.entity.User;
import static com.jjmsoftsolutions.model.GroupModel.getGroupsLeader;
import static com.jjmsoftsolutions.model.LeaderModel.getLeaderByUserId;
import static com.jjmsoftsolutions.model.UserModel.insert;
import static com.jjmsoftsolutions.model.UserModel.update;
import static com.jjmsoftsolutions.utils.Context.showMessage;
import com.jjmsoftsolutions.utils.Session;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "userBean")
@ViewScoped
public class UserBean implements Serializable {

    private User user;
    private Groups selectedGroup = null;
    private List<Groups> groups = null;
    private UserDataModel userDataModel = null;
    private User selectedUser = null;
    private GroupConverter converter = null;

    public UserBean() {
        init();
    }

    private void init() {
        converter = new GroupConverter(getGroupsList());
        groups = converter.getList();
        user = new User();
        userDataModel = new UserDataModel();
    }

    private List<Groups> getGroupsList() {
        return getGroupsLeader(Session.getUser().getIdUser());
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the selectedGroup
     */
    public Groups getSelectedGroup() {
        return selectedGroup;
    }

    /**
     * @param selectedGroup the selectedGroup to set
     */
    public void setSelectedGroup(Groups selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    /**
     * @return the groups
     */
    public List<Groups> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }

    /**
     * @return the userDataModel
     */
    public UserDataModel getUserDataModel() {
        return userDataModel;
    }

    /**
     * @param userDataModel the userDataModel to set
     */
    public void setUserDataModel(UserDataModel userDataModel) {
        this.userDataModel = userDataModel;
    }

    /**
     * @return the selectedUser
     */
    public User getSelectedUser() {
        return selectedUser;
    }

    /**
     * @param selectedUser the selectedUser to set
     */
    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public void search() {
        List<User> list = null;
        if (selectedGroup != null) {
            if (user.getFirstName().trim().length() > 0) {
                list = getUsersByGroupAndFirstName(selectedGroup.getUserList(), user.getFirstName());
            } else {
                list = selectedGroup.getUserList();
            }
        } else {
            showMessage("", "Seleccione una célula", false);
        }

        userDataModel = new UserDataModel();
        userDataModel.setData(list);
        converter.setList(getGroupsList());

    }

    public void delete() {
        if (selectedUser != null) {
            selectedUser.setIdGroup(null);
            boolean result = update(selectedUser);
            showMessage("Usuario eliminado satisfactoriamente", "Error al eliminar al usuario", result);
        } else {
            showMessage("", "Seleccione un usuario", false);
        }
        showMessage("", "Solo el lider puede eliminar a su discipulo", false);
        search();
    }

    public void insertUserGroup() {
        User userSession = Session.getUser();        
        user.setIdCompany(userSession.getIdCompany());
        user.setIdSubred(userSession.getIdSubred());
        user.setIdGroup(selectedGroup);
        user.setIdRole(new Role(1));
        boolean result = insert(user);
        showMessage("Ingresado Satisfacoriamente", "Error al ingresar al usuario", result);
        user = new User();
    }

    public void insertUserConsolidation() {
        user.setIdRole(new Role(1));
        user.setIdCompany(Session.getCompany());
        user.setIdSubred(Session.getSubRed());
        boolean result = insert(user);
        showMessage("Ingresado Satisfacoriamente", "Error al ingresar al usuario", result);
        user = new User();
    }

    public void hide() {
        init();
    }
}
