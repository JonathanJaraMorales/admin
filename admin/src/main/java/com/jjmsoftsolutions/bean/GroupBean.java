package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.controller.ProvinceController;
import com.jjmsoftsolutions.entity.Company;
import com.jjmsoftsolutions.entity.District;
import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.entity.Leader;
import com.jjmsoftsolutions.entity.LeaderPK;
import com.jjmsoftsolutions.entity.SubRed;
import com.jjmsoftsolutions.entity.User;
import com.jjmsoftsolutions.model.GroupModel;
import com.jjmsoftsolutions.model.LeaderModel;
import com.jjmsoftsolutions.utils.Context;
import com.jjmsoftsolutions.utils.Session;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "groupBean")
@ViewScoped
public class GroupBean implements Serializable{

    private Groups group;
    private ProvinceController provinceController = null;

    public GroupBean() {
        group = new Groups();
        provinceController = new ProvinceController();
    }

    /**
     * @return the group
     */
    public Groups getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(Groups group) {
        this.group = group;
    }

    /**
     * @return the district
     */
    public District getDistrict() {
        return provinceController.getCantonController().getDistrictController().getSelectedDistrict();
    }

    public void insert() {
        User user = Session.getUser();
        Company company = user.getIdCompany();
        SubRed subRed = user.getIdSubred();

        group.setIdCompany(company);
        group.setIdDistrict(getDistrict());
        group.setIdSubred(subRed);

        Groups g = GroupModel.doInsert(group);

        if (g != null) {
            Leader leader = new Leader();
            leader.setIdCompany(company);
            leader.setLeaderPK(new LeaderPK(g.getIdGroup(), user.getIdUser()));
            boolean result = LeaderModel.insert(leader);
            Context.showMessage("Ingresada satisfactoriamente la célula", "Error al ingresar la célula", result);
        } else {
            Context.showMessage("", "Error al ingresar la célula", false);
        }
        
        group = new Groups();
    }

    /**
     * @return the provinceController
     */
    public ProvinceController getProvinceController() {
        return provinceController;
    }

    /**
     * @param provinceController the provinceController to set
     */
    public void setProvinceController(ProvinceController provinceController) {
        this.provinceController = provinceController;
    }

}
