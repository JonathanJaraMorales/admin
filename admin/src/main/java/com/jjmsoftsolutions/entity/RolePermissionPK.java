/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luis
 */
@Embeddable
public class RolePermissionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ROLE")
    private int idRole;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PERMISSION")
    private int idPermission;

    public RolePermissionPK() {
    }

    public RolePermissionPK(int idRole, int idPermission) {
        this.idRole = idRole;
        this.idPermission = idPermission;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public int getIdPermission() {
        return idPermission;
    }

    public void setIdPermission(int idPermission) {
        this.idPermission = idPermission;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idRole;
        hash += (int) idPermission;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePermissionPK)) {
            return false;
        }
        RolePermissionPK other = (RolePermissionPK) object;
        if (this.idRole != other.idRole) {
            return false;
        }
        if (this.idPermission != other.idPermission) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.RolePermissionPK[ idRole=" + idRole + ", idPermission=" + idPermission + " ]";
    }
    
}
