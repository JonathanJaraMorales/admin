package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.entity.Province;
import com.jjmsoftsolutions.model.ProvinceModel;
import java.io.Serializable;
import java.util.List;

public class ProvinceController implements Serializable {

    private Province selectedProvince;
    private List<Province> provinces;

    private CantonController cantonController = null;

    public ProvinceController() {
        provinces = new ProvinceModel().getProvinceList();
        cantonController = new CantonController();
    }

    /**
     * @return the selectedProvince
     */
    public Province getSelectedProvince() {
        return selectedProvince;
    }

    /**
     * @param selectedProvince the selectedProvince to set
     */
    public void setSelectedProvince(Province selectedProvince) {
        this.selectedProvince = selectedProvince;
    }

    /**
     * @return the provinces
     */
    public List<Province> getProvinces() {
        return provinces;
    }

    /**
     * @param provinces the provinces to set
     */
    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }

    /**
     * @return the cantonController
     */
    public CantonController getCantonController() {
        if(selectedProvince != null){
            cantonController.setProvince(selectedProvince);
        }
        return cantonController;
    }

    /**
     * @param cantonController the cantonController to set
     */
    public void setCantonController(CantonController cantonController) {
        this.cantonController = cantonController;
    }

}
