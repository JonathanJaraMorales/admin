package com.jjmsoftsolutions.model;

import static com.jjmsoftsolutions.dataacces.DataAccess.findAll;
import com.jjmsoftsolutions.entity.District;
import java.util.List;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class DistrictModel {

    /**
     * Search the <strong>All</strong> districts
     * @return transaction state
     */
    public List<District> getDistrictList() {
        List<District> list = (List) findAll(District.class);
        return list;
    }

}
