package com.jjmsoftsolutions.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import static java.util.logging.Logger.getLogger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import static org.apache.commons.net.ftp.FTPReply.isPositiveCompletion;

/**
 *
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class FtpUtil {

    private static final String FTP_USER = "public@jjmsoftsolutions.com";
    private static final String FTP_PASSWORD = "sp*pb1Cmp~dD";
    private static final String FTP_HOST = "ftp.jjmsoftsolutions.com";

    /**
     * Upload a file in the server
     * @param directory: Specify the root
     * @param inputStream: File to upload
     * @param fileName: Name's file how appears in the server
     * @return 
     */
    public static boolean upload(String directory, InputStream inputStream, String fileName) {
        try {      
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(FTP_HOST);
            ftpClient.login(FTP_USER, FTP_PASSWORD);
            if (!isPositiveCompletion(ftpClient.getReplyCode())) {
                ftpClient.disconnect();
            } else {                  
                ftpClient.changeWorkingDirectory(directory);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                BufferedInputStream buffIn = new BufferedInputStream(inputStream);
                ftpClient.enterLocalPassiveMode();
                boolean res = ftpClient.storeFile(fileName, buffIn);
                buffIn.close(); 
                ftpClient.logout(); 
                ftpClient.disconnect();                
                return res;
            }
        } catch (IOException ex) {
            getLogger(FtpUtil.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return false;
    }

    /**
     * Create a folder in a specific root in the server
     * @param directory: 
     * @param folderName
     * @return 
     */
    public static boolean createFolder(String directory, String folderName) {
        try {      
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(FTP_HOST);
            ftpClient.login(FTP_USER, FTP_PASSWORD);
            if (!isPositiveCompletion(ftpClient.getReplyCode())) {
                ftpClient.disconnect();
            } else {
                ftpClient.changeWorkingDirectory(directory);
                ftpClient.makeDirectory(folderName);
            }
        } catch (IOException ex) {
            getLogger(FtpUtil.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return false;    
    }
}
