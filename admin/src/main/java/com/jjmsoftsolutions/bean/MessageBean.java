package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.Converter.GroupConverter;
import com.jjmsoftsolutions.datamodel.UserDataModel;
import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.entity.Message;
import com.jjmsoftsolutions.entity.User;
import com.jjmsoftsolutions.entity.UserMessage;
import com.jjmsoftsolutions.entity.UserMessagePK;
import static com.jjmsoftsolutions.model.GroupModel.getGroupsLeader;
import com.jjmsoftsolutions.model.MessageModel;
import com.jjmsoftsolutions.utils.Session;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "messageBean")
@ViewScoped
public class MessageBean implements Serializable {

    private Groups selectedGroup = null;
    private Message message = null;
    private List<User> selectedUsers = null;
    private List<User> users = null;
    private GroupConverter converter = null;
    private List<Groups> groups = null;

    public MessageBean() {
        message = new Message();
        converter = new GroupConverter(getGroupsList());
        groups = converter.getList();
    }

    private List<Groups> getGroupsList() {
        return getGroupsLeader(Session.getUser().getIdUser());
    }

    public void addMessage() {
        getMessage().setIdCompany(Session.getCompany());
        getMessage().setIdGroup(getSelectedGroup());
        setMessage(MessageModel.insert(getMessage()));
        for (User user : getSelectedUsers()) {
            UserMessage usrMessage = new UserMessage();
            usrMessage.setIdCompany(Session.getCompany());
            usrMessage.setUser(user);
            usrMessage.setMessage(getMessage());
            usrMessage.setUserMessagePK(new UserMessagePK(user.getIdUser(), getMessage().getIdMessage()));
            MessageModel.insertMessageUser(usrMessage);
        }
    }

    public List<User> getUsers() {
        if (getSelectedGroup() != null) { 
            users = getSelectedGroup().getUserList();
        }
        return users;

    }

    /**
     * @return the selectedGroup
     */
    public Groups getSelectedGroup() {
        return selectedGroup;
    }

    /**
     * @param selectedGroup the selectedGroup to set
     */
    public void setSelectedGroup(Groups selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    /**
     * @return the selectedUsers
     */
    public List<User> getSelectedUsers() {
        return selectedUsers;
    }

    /**
     * @param selectedUsers the selectedUsers to set
     */
    public void setSelectedUsers(List<User> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    /**
     * @return the message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * @return the converter
     */
    public GroupConverter getConverter() {
        return converter;
    }

    /**
     * @param converter the converter to set
     */
    public void setConverter(GroupConverter converter) {
        this.converter = converter;
    }

    /**
     * @return the groups
     */
    public List<Groups> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }

    public void change() {
        if (selectedGroup != null) {
            users = selectedGroup.getUserList();
        }
    }

}
