/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luis
 */
@Embeddable
public class UserMessagePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USER")
    private int idUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_MESSAGE")
    private int idMessage;

    public UserMessagePK() {
    }

    public UserMessagePK(int idUser, int idMessage) {
        this.idUser = idUser;
        this.idMessage = idMessage;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUser;
        hash += (int) idMessage;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserMessagePK)) {
            return false;
        }
        UserMessagePK other = (UserMessagePK) object;
        if (this.idUser != other.idUser) {
            return false;
        }
        if (this.idMessage != other.idMessage) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.UserMessagePK[ idUser=" + idUser + ", idMessage=" + idMessage + " ]";
    }
    
}
