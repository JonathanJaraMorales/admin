package com.jjmsoftsolutions.model;

import static com.jjmsoftsolutions.dataacces.DataAccess.find;
import com.jjmsoftsolutions.entity.User;
import java.util.HashMap;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class LoginModel {

    /**
     * Validate if the user exist in the database
     * @param userName
     * @param password
     * @return 
     */
    public User login(String userName, String password) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM User U ");
        sql.append("WHERE U.username = :username ");
        sql.append("AND U.password = :password ");

        HashMap parameters = new HashMap();
        parameters.put("username", userName);
        parameters.put("password", password);        

        User user = (User) find(User.class, sql.toString(), parameters);

        return user;
    }

}
