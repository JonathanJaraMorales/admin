/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.datamodel;

import com.jjmsoftsolutions.entity.User;
import com.jjmsoftsolutions.utils.PrimeDataModel;
import com.jjmsoftsolutions.utils.StringUtils;
import static com.jjmsoftsolutions.utils.StringUtils.getInt;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author jonathan
 */
public class UserDataModel extends PrimeDataModel<User> implements SelectableDataModel<User>{
    
    public UserDataModel(List<User> data){
        super(data);
    }
    
    public UserDataModel(){
        this(null);
    }
    
    public void setData(List<User> data){
        setWrappedData(data);
    }

    @Override
    public Object getRowKey(User user) {
        return user.getIdUser();
    }

    @Override
    public User getRowData(String rowKey) {
        List<User> users = (List<User>) getWrappedData();
        int id = getInt(rowKey);         
        for(User user : users) {  
            if(user.getIdUser() == id)  
                return user;  
        }
        return null; 
    }
    
}
