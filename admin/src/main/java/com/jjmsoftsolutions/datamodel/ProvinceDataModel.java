/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.datamodel;

import com.jjmsoftsolutions.entity.Province;
import com.jjmsoftsolutions.utils.PrimeDataModel;
import java.util.List;

/**
 *
 * @author luis
 */
public class ProvinceDataModel extends PrimeDataModel<Province>{
    
    public ProvinceDataModel(Object data){
        super(data);
    }
    
      public Province getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data
        
        List<Province> pictures = (List<Province>) getWrappedData();
        
        for(Province picture : pictures) {
            if(picture.getName().equals(rowKey))
                return picture;
        }
        
        return null;
    }

    public Object getRowKey(Province picture) {
        return picture.getIdProvince();
    }
    
}
