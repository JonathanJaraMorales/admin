package com.jjmsoftsolutions.model;

import static com.jjmsoftsolutions.dataacces.DataAccess.findAll;
import com.jjmsoftsolutions.entity.Canton;
import java.util.List;

/**
 *
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class CantonModel {

    /**
     * Search the <strong>ALL</strong> canton 
     * @return A Canton List
     */
    public List<Canton> getCantonList() {
        List<Canton> list = (List) findAll(Canton.class);
        return list;
    }
    
}
