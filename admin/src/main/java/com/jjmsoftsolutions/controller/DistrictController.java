/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.entity.Canton;
import com.jjmsoftsolutions.entity.District;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "districtController")
@ViewScoped
public class DistrictController implements Serializable{

    private Canton canton;

    private District selectedDistrict;
    private List<District> districts;

    private GroupController groupController;

    public DistrictController() {
        groupController = new GroupController();
    }

    /**
     * @return the selectedDistrict
     */
    public District getSelectedDistrict() {
        return selectedDistrict;
    }

    /**
     * @param selectedDistrict the selectedDistrict to set
     */
    public void setSelectedDistrict(District selectedDistrict) {
        this.selectedDistrict = selectedDistrict;
    }

    /**
     * @return the districts
     */
    public List<District> getDistricts() {
        if (canton != null) {
            setDistricts(canton.getDistrictList());
            return districts;
        } else {
            return null;
        }
    }

    /**
     * @param districts the districts to set
     */
    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    /**
     * @return the canton
     */
    public Canton getCanton() {
        return canton;
    }

    /**
     * @param canton the canton to set
     */
    public void setCanton(Canton canton) {
        this.canton = canton;
    }

    /**
     * @return the groupController
     */
    public GroupController getGroupController() {
        if (selectedDistrict != null) {
            groupController.setDistrict(getSelectedDistrict());
        }
        return groupController;
    }

    /**
     * @param groupController the groupController to set
     */
    public void setGroupController(GroupController groupController) {
        this.groupController = groupController;
    }

}
