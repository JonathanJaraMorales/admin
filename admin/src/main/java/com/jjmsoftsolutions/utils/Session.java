package com.jjmsoftsolutions.utils;

import com.jjmsoftsolutions.entity.Company;
import com.jjmsoftsolutions.entity.SubRed;
import com.jjmsoftsolutions.entity.User;
import java.io.IOException;
import java.util.logging.Level;
import static java.util.logging.Logger.getLogger;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class Session {

    /**
     * Search the user in seesion
     * @return 
     */
    public static User getUser() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        return (User) session.getAttribute("user");
    }
    
    /**
     * Search the company in seesion
     * @return 
     */
    public static Company getCompany() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        return ((User) session.getAttribute("user")).getIdCompany();
    }
    
      /**
     * Search the company in seesion
     * @return 
     */
    public static SubRed getSubRed() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        return ((User) session.getAttribute("user")).getIdSubred();
    }

    /**
     * Search the session object
     * @return 
     */
    public static HttpSession getSession() {
        return (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
    }

    /**
     * Redirect to default page when the user is logged
     */
    public static void redirect() {
        try {
            getCurrentInstance().getExternalContext().redirect("faces/Desktop.xhtml");
        } catch (IOException ex) {
            getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
