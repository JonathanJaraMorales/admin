package com.jjmsoftsolutions.reports;

import com.jjmsoftsolutions.model.MessageModel;
import com.jjmsoftsolutions.utils.Dates;
import java.util.Date;
import java.util.HashMap;

public class MensualReport extends MasterReport {

 
    public MensualReport() {
        super(null, "ReporteCelula");
    }

    public void printPDF(int month, int year) {
        super.PDF(getParameters(month, year));
    }

    private HashMap<String, Object> getParameters(int month, int year) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("MONTH", Dates.getMonthName(month));
        parameters.put("LEADER", "");
        parameters.put("LEADER_SUPERIOR", "");
        parameters.put("LEADER_TEL", "");
        parameters.put("LEADER_SUPERIOR_TEL", "");
        parameters.put("AGE", "");
        Date[] dates = getDate(month, year);
        MessageModel.getMessagesByMonth(dates[0], dates[1]);
        parameters.put("HOUR_1", "");
        parameters.put("THEME_1", "");
        parameters.put("MONEY_1", "");
        return parameters;
    }

    private Date[] getDate(int month, int year) {
        Date[] dates = new Date[2];
        dates[0] = Dates.getFirstDay(month, year);
        dates[1] = Dates.getLastDay(month, year);
        return dates;
    }
    


}
