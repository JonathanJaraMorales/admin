package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Consolidation;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class ConsolidationModel {
    
    /**
     * Insert an cosolidation objet in the DataBase
     * @param consolidation
     * @return transacction state
     */
    public static boolean insert(Consolidation consolidation){
        return DataAccess.insert(consolidation);
    }
    
}
