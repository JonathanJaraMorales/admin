package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import static com.jjmsoftsolutions.dataacces.DataAccess.findAll;
import com.jjmsoftsolutions.entity.User;
import java.util.HashMap;
import java.util.List;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class UserModel {
    
    /**
     * Insert an user in the DataBase
     * @param user to insert
     * @return transaction state
     */
    public static boolean insert(User user) {
        return DataAccess.insert(user);
    }

    /**
     * Update an user
     * @param user to update
     * @return transaction state
     */
    public static boolean update(User user) {
        return DataAccess.update(user);
    }

    /**
     * Search the user without leader
     * @return 
     */
    public static List<User> getUserWithoutLeader() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM User U ");
        sql.append("WHERE U.idGroup IS NULL ");
        List<User> list = (List) findAll(User.class, sql.toString());
        return list;
    }

    /**
     * Search the user with leader
     * @return 
     */
    public static List<User> getUserWithLeader() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM User U ");
        sql.append("WHERE U.idLeader IS NOT NULL ");
        List<User> list = (List) findAll(User.class, sql.toString());
        return list;
    }

    /**
     * Search an User by id
     * @param id
     * @return 
     */
    public static User find(Integer id) {
        return (User) DataAccess.find(User.class, id);
    }

    /**
     * Search the <strong>All</strong> users
     * @return 
     */
    public static List<User> getAllUsers() {
        List<User> list = (List) findAll(User.class);
        return list;
    }

    /**
     * Search the <strong>All</strong> with a specific First Name
     * @param firstName
     * @return 
     */
    public static List<User> getUserByName(String firstName) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM User U ");
        sql.append("WHERE U.firstName = :firstName");
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("firstName", firstName);
        List<User> list = (List) findAll(User.class, sql.toString(),parameters);
        return list;
    }
    
    /**
     * Delete an user in the DataBase
     * @param user
     * @return 
     */
    public static boolean delete(User user){
        DataAccess.update(user);
        return DataAccess.delete(User.class, user.getIdUser());
    }
}
