/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "ROLE_PERMISSION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolePermission.findAll", query = "SELECT r FROM RolePermission r"),
    @NamedQuery(name = "RolePermission.findByIdRole", query = "SELECT r FROM RolePermission r WHERE r.rolePermissionPK.idRole = :idRole"),
    @NamedQuery(name = "RolePermission.findByIdPermission", query = "SELECT r FROM RolePermission r WHERE r.rolePermissionPK.idPermission = :idPermission")})
public class RolePermission implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RolePermissionPK rolePermissionPK;
    @JoinColumn(name = "ID_COMPANY", referencedColumnName = "ID_COMPANY")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company idCompany;
    @JoinColumn(name = "ID_PERMISSION", referencedColumnName = "ID_PERMISSION", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Permission permission;
    @JoinColumn(name = "ID_ROLE", referencedColumnName = "ID_ROLE", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Role role;

    public RolePermission() {
    }

    public RolePermission(RolePermissionPK rolePermissionPK) {
        this.rolePermissionPK = rolePermissionPK;
    }

    public RolePermission(int idRole, int idPermission) {
        this.rolePermissionPK = new RolePermissionPK(idRole, idPermission);
    }

    public RolePermissionPK getRolePermissionPK() {
        return rolePermissionPK;
    }

    public void setRolePermissionPK(RolePermissionPK rolePermissionPK) {
        this.rolePermissionPK = rolePermissionPK;
    }

    public Company getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Company idCompany) {
        this.idCompany = idCompany;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolePermissionPK != null ? rolePermissionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePermission)) {
            return false;
        }
        RolePermission other = (RolePermission) object;
        if ((this.rolePermissionPK == null && other.rolePermissionPK != null) || (this.rolePermissionPK != null && !this.rolePermissionPK.equals(other.rolePermissionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.RolePermission[ rolePermissionPK=" + rolePermissionPK + " ]";
    }
    
}
