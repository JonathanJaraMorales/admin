/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.datamodel;

import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.utils.PrimeDataModel;
import com.jjmsoftsolutions.utils.StringUtils;
import static com.jjmsoftsolutions.utils.StringUtils.getInt;
import java.util.List;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author jonathan
 */
public class GroupDataModel extends PrimeDataModel<Groups> implements SelectableDataModel<Groups>{
 
     
    public GroupDataModel(List<Groups> data){
        super(data);
    }

    @Override
    public Object getRowKey(Groups group) {
        return group.getIdGroup();
    }

    @Override
    public Groups getRowData(String rowKey) {
        List<Groups> groups = (List<Groups>) getWrappedData();
        int id = getInt(rowKey);         
        for(Groups group : groups) {  
            if(group.getIdGroup() == id)  
                return group;  
        }
        return null; 
    }
}
