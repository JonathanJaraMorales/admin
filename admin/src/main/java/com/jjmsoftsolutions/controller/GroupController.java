package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.datamodel.GroupDataModel;
import com.jjmsoftsolutions.entity.District;
import com.jjmsoftsolutions.entity.Groups;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "groupController")
@ViewScoped
public final class GroupController implements Serializable {

    private GroupDataModel groupDataModel;
    private Groups selectedGroup;
    private Groups group;
    private District district;
    
    public GroupController( ) {
        group = new Groups();
    }

    public Groups getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(Groups selectedGroup) {
       this.selectedGroup = selectedGroup;
    }
    
    public GroupDataModel getGroupDataModel() {
        if (district!= null) {
            setGroupDataModel(new GroupDataModel(district.getGroupsList()));
            return groupDataModel;
        }
        return null;
    }

    public void setGroupDataModel(GroupDataModel groupDataModel) {
        this.groupDataModel = groupDataModel;
    }
    
    public Groups getGroup() {
        return group;
    }

    public void setGroup(Groups group) {
        this.group = group;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

}
