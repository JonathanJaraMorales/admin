package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Leader;
import com.jjmsoftsolutions.entity.User;
import static com.jjmsoftsolutions.model.GroupModel.findGroupById;
import static com.jjmsoftsolutions.model.UserModel.find;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class LeaderModel {

    /**
     * Insert the leader in the database
     *
     * @param leader to insert
     * @return transaction status
     */
    public static boolean insert(Leader leader) {
        return DataAccess.insert(leader);
    }

    /**
     * Search for the leader of a user
     *
     * @param id
     * @return
     */
    public static Leader getLeaderByUserId(Integer id) {
        //return find(id).getIdLeader();
        return null;
    }

    /**
     * Search for the leader of a group
     *
     * @param idGroup
     * @return
     */
    public static Leader getLeaderByGroup(Integer idGroup) {
        //return findGroupById(idGroup).getIdLeader();
        return null;
    }

    /**
     * Search if the user is a leader
     *
     * @param idUser
     * @return
     */
    public static Leader getIdLeader(Integer idUser) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT leaderPK.idLeader FROM Leader l ");
        sql.append("WHERE l.idUser.idUser = :idUser ");
        HashMap parameters = new HashMap();
        parameters.put("idUser", idUser);

        Leader leader = (Leader) DataAccess.find(Leader.class, sql.toString(), parameters);

        return leader;
    }

    public static List<Leader> getLeader(Integer idUser) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT L FROM Leader l ");
        sql.append("WHERE l.leaderPK.idUser.idUser = :idUser ");
        HashMap parameters = new HashMap();
        parameters.put("idUser", idUser);

        Leader leader = (Leader) DataAccess.find(Leader.class, sql.toString(), parameters);
        List<Leader> list = new ArrayList();
        list.add(leader);
        
        return list;
        
    }

}
