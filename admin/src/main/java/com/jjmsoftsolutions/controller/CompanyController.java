/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.entity.Company;
import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.utils.Session;
import static com.jjmsoftsolutions.utils.Session.getUser;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "companyController")
@ViewScoped
public class CompanyController {
   
    private Company selectedCompany;
    private Groups group;

    public CompanyController(){
        selectedCompany = getUser().getIdGroup().getIdSubred().getIdCompany();
    }
     /**
     * @return the selectedCompany
     */
    public Company getSelectedCompany() {
        return selectedCompany;
    }

    /**
     * @param selectedCompany the selectedCompany to set
     */
    public void setSelectedCompany(Company selectedCompany) {
        this.selectedCompany = selectedCompany;
    }

    /**
     * @return the group
     */
    public Groups getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(Groups group) {
        this.group = group;
    }
    
    
    
    
}
