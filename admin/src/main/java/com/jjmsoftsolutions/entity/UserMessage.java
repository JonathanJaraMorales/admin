/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "USER_MESSAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserMessage.findAll", query = "SELECT u FROM UserMessage u"),
    @NamedQuery(name = "UserMessage.findByIdUser", query = "SELECT u FROM UserMessage u WHERE u.userMessagePK.idUser = :idUser"),
    @NamedQuery(name = "UserMessage.findByIdMessage", query = "SELECT u FROM UserMessage u WHERE u.userMessagePK.idMessage = :idMessage")})
public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserMessagePK userMessagePK;
    @JoinColumn(name = "ID_COMPANY", referencedColumnName = "ID_COMPANY")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company idCompany;
    @JoinColumn(name = "ID_MESSAGE", referencedColumnName = "ID_MESSAGE", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Message message;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    public UserMessage() {
    }

    public UserMessage(UserMessagePK userMessagePK) {
        this.userMessagePK = userMessagePK;
    }

    public UserMessage(int idUser, int idMessage) {
        this.userMessagePK = new UserMessagePK(idUser, idMessage);
    }

    public UserMessagePK getUserMessagePK() {
        return userMessagePK;
    }

    public void setUserMessagePK(UserMessagePK userMessagePK) {
        this.userMessagePK = userMessagePK;
    }

    public Company getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Company idCompany) {
        this.idCompany = idCompany;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userMessagePK != null ? userMessagePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserMessage)) {
            return false;
        }
        UserMessage other = (UserMessage) object;
        if ((this.userMessagePK == null && other.userMessagePK != null) || (this.userMessagePK != null && !this.userMessagePK.equals(other.userMessagePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.UserMessage[ userMessagePK=" + userMessagePK + " ]";
    }
    
}
