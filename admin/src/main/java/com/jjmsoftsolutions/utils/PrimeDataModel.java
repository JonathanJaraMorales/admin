package com.jjmsoftsolutions.utils;

import java.io.Serializable;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 * 
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014
 * @param <T> 
 * Copyright 2014 JJMSoftSolutions
 */
public class PrimeDataModel<T> extends ListDataModel implements SelectableDataModel<T>, Serializable {
            
    public PrimeDataModel() {}
    
    public PrimeDataModel(Object data) {
        setWrappedData(data);
    }
    
    public Object getRowKey(T object) {
        throw new UnsupportedOperationException("Must be implemented");
    }
    
    public T getRowData(String rowKey) {
        throw new UnsupportedOperationException("Must be implemented");
    }
}
