package com.jjmsoftsolutions.utils;

import javax.faces.application.FacesMessage;
import static javax.faces.context.FacesContext.getCurrentInstance;

/**
 *
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class Context {
    
    /**
     * Dispalys a message dialog in the scren with the transaction state
     * @param msgSucceful: Specify the mesage if the transaction is succes
     * @param msgError: Specify the message if the transaction is wrong
     * @param result:Specify the transaction state
     */
    public static void showMessage(String msgSucceful, String msgError, boolean result){
        if (result) {
            getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", msgSucceful));
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", msgError));
        }
    }    
}
