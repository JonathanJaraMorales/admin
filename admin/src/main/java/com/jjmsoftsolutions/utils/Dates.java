/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author luis
 */
public class Dates {

    public static String getMonthName(int month) {
        String name = "";
        if (month == 0) {
            name = "Enero";
        } else if (month == 1) {
            name = "Febrero";
        } else if (month == 2) {
            name = "Marzo";
        } else if (month == 3) {
            name = "Abril";
        } else if (month == 4) {
            name = "Mayo";
        } else if (month == 5) {
            name = "Junio";
        } else if (month == 6) {
            name = "Julio";
        } else if (month == 7) {
            name = "Agosto";
        } else if (month == 8) {
            name = "Setiembre";
        } else if (month == 9) {
            name = "Octubre";
        } else if (month == 10) {
            name = "Noviembre";
        } else if (month == 11) {
            name = "Diciembre";
        }
        return name;
    }

    public static Date getFirstDay(int month, int year) {
        GregorianCalendar toDate = new GregorianCalendar();
        toDate.set(Calendar.MONTH, month);
        toDate.set(Calendar.YEAR, year);
        toDate.set(Calendar.DAY_OF_MONTH, 1);
        return toDate.getTime();
    }

    public static Date getLastDay(int month, int year) {
        GregorianCalendar fromDate = new GregorianCalendar();
        fromDate.set(Calendar.YEAR, year);
        fromDate.set(Calendar.MONTH, month);

        fromDate.set(fromDate.get(Calendar.YEAR),
                fromDate.get(Calendar.MONTH),
                fromDate.getActualMaximum(Calendar.DAY_OF_MONTH),
                fromDate.getMaximum(Calendar.HOUR_OF_DAY),
                fromDate.getMaximum(Calendar.MINUTE),
                fromDate.getMaximum(Calendar.SECOND));
        return fromDate.getTime();
    }

}
