package com.jjmsoftsolutions.bean;

import static com.jjmsoftsolutions.controller.ConsolidationController.insert;
import com.jjmsoftsolutions.controller.ProvinceController;
import com.jjmsoftsolutions.datamodel.UserDataModel;
import com.jjmsoftsolutions.domain.Email;
import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.entity.Leader;
import com.jjmsoftsolutions.entity.User;
import static com.jjmsoftsolutions.model.UserModel.getUserWithoutLeader;
import static com.jjmsoftsolutions.model.UserModel.update;
import com.jjmsoftsolutions.utils.Context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "assign")
@ViewScoped
public class AssignBean implements Serializable {

    private ProvinceController provinceController = null;
    private UserDataModel userDataModel = null;
    private User selectedUser = null;
    private String comments;

    public AssignBean() {
        init();
    }

    private void init() {
        provinceController = new ProvinceController();
        userDataModel = new UserDataModel(getUserWithoutLeader());
        comments = "";
    }

    public ProvinceController getProvinceController() {
        return provinceController;
    }

    public void setProvinceController(ProvinceController provinceController) {
        this.provinceController = provinceController;
    }

    public UserDataModel getUserDataModel() {
        return userDataModel;
    }

    public void setUserDataModel(UserDataModel userDataModel) {
        this.userDataModel = userDataModel;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    private Groups getSelectedGroup() {
        return provinceController.getCantonController().getDistrictController().getGroupController().getSelectedGroup();
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void assign() {
        if (validate()) {
            boolean result = beginTransactions();

            if (result) {
                List<User> userList = new ArrayList<>();
                List<Leader> leaders = getSelectedGroup().getLeaderList();
                for (Leader leader : leaders) {
                    userList.add(leader.getUser());
                }
                Email email = new Email();
                for (User user : userList) {
                    boolean emailSend = email.send(user.getEmail(), "Consolidación", getEmailMessage(), "email");
                    boolean smsSend = email.sendSms("" + user.getCellphone(), getSMS());
                    Context.showMessage("Enviado el correo el mensaje de correo electrónico a " + user.getEmail(), "Error al enviar el mensaje de correo electrónico" + user.getEmail(), emailSend);
                    Context.showMessage("Enviado el mensaje de texto al número " + user.getCellphone(), "Error al enviar el mensaje de texto al número " + user.getCellphone(), smsSend);
                }
                getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro satisfactorio ", "Se ha enviado la información satisfactoriamente"));
            } else {
                getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registro Incompleto ", "No se ha enviado la información"));
            }
            init();
        }
    }

    private boolean beginTransactions() {
        boolean transactionConsolidation = insert(getComments(), getSelectedGroup(), selectedUser, new Date());
        selectedUser.setIdGroup(getSelectedGroup());
        boolean transactionUser = update(selectedUser);
        return transactionConsolidation && transactionUser;
    }

    private boolean validate() {
        boolean result = true;
        if (getSelectedGroup() == null) {
            result = false;
            Context.showMessage("", "Seleccione una célula", result);
            result = false;
        } else if (selectedUser == null) {
            Context.showMessage("", "Seleccione una usuario", result);
        }
        return result;
    }

    private String getEmailMessage() {
        StringBuilder html = new StringBuilder();
        html.append("<html>");
        html.append("<head>");
        html.append("</head>");
        html.append("<body>");
        html.append("<div id=\"header\">");
        html.append("Informe de Consolidación");
        html.append("</div>");
        html.append("<div id=\"content\">");
        html.append("<span>Nombre:</span>").append(selectedUser.getFirstName()).append("<br/>");
        html.append("<span>Primer Apellido:</span>").append(selectedUser.getLastName()).append("<br/>");
        html.append("<span>Segundo Apellido:</span>").append(selectedUser.getSecondLastName()).append("<br/>");
        html.append("<span>Teléfono Habitación:</span>").append(selectedUser.getPhone()).append("<br/>");
        html.append("<span>Teléfono Celular:</span>").append(selectedUser.getCellphone()).append("<br/>");
        html.append("<span>Célula Asignada:</span>").append(getSelectedGroup().getAddress()).append("<br/>");
        html.append("<span>Comentarios:</span>").append(comments).append("<br/>");
        html.append("</div>");
        html.append("<body>");
        html.append("</html>");
        return html.toString();
    }

    private String getSMS() {
        StringBuilder sms = new StringBuilder();
        sms.append("Has recibido un nuevo discípulo llamado ").append(selectedUser.getFirstName()).append(" ").append(selectedUser.getLastName()).append(" ").append(selectedUser.getSecondLastName()).append(". ");
        if (selectedUser.getCellphone() > 0) {
            sms.append("Favor contactar al ").append(selectedUser.getCellphone()).append(" ");
        }
        if (selectedUser.getPhone() > 0) {
            sms.append("Favor contactar al ").append(selectedUser.getPhone()).append(" ");
        }
        if (selectedUser.getAddress() != null && selectedUser.getAddress().trim().length() > 0) {
            sms.append("El discípulo vive en: ").append(selectedUser.getAddress()).append(" ");
        }
        if (selectedUser.getAge() > 0) {
            sms.append("Tiene una edad de: ").append(selectedUser.getAge()).append(" ");
        }
        sms.append("Este mensaje es enviado desde un servidor, favor no responder el mensaje. ");
        return sms.toString();
    }

    public void hide() {
        init();
    }
}
