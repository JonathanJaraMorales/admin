package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import static com.jjmsoftsolutions.dataacces.DataAccess.find;
import static com.jjmsoftsolutions.dataacces.DataAccess.findAll;
import com.jjmsoftsolutions.entity.Groups;
import java.util.HashMap;
import java.util.List;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class GroupModel {

    /**
     * Search the group in some District
     * @return Group List
     */
    public static List<Groups> getGroupByDistrict() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Groups U ");
        sql.append("WHERE U.idGroup = :idGroup ");
        HashMap parameters = new HashMap();
        parameters.put("idGroup", "1");
        List<Groups> list = (List) findAll(Groups.class);
        return list;
    }
    
    /**
     * Insert a Group in the DataBase
     * @param group to insert
     * @return 
     */
    public static boolean insert(Groups group){
        return DataAccess.insert(group);        
    }
    
    public static Groups doInsert(Groups groups){
        return (Groups)DataAccess.doInsert(groups);
    }
    
    /**
     * Search the <strong>All</strong> groups by <strong>Leader</strong>
     * @param idLeader
     * @return 
     */
    public static List<Groups> getGroupsLeader(Integer idLeader){
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT groups FROM Leader U ");
        sql.append("WHERE U.leaderPK.idUser = :idLeader ");
        HashMap parameters = new HashMap();
        parameters.put("idLeader", idLeader);
        List<Groups> list = (List) findAll(Groups.class, sql.toString(), parameters);
        return list;
    }
    
    /**
     * Search a group by id
     * @param id
     * @return 
     */
    public static Groups findGroupById(Integer id){
        return (Groups)find(Groups.class, id);
    }

}
