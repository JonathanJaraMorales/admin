package com.jjmsoftsolutions.utils;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

/**
 *
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class StringUtils {

    /**
     *Allow parse an Object to integer value
     * @param value
     * @return
     */
    public static int getInt(Object value) {
        int toReturn = 0;
        String val = getString(value).replaceAll(",", "");
        try {
            if (val.trim().length() > 0) {
                if (val.contains(".")) {
                    toReturn = (int) parseDouble(val);
                } else {
                    toReturn = parseInt(val);
                }
            }
        } catch (NumberFormatException e) {
            toReturn = 0;
        }
        return toReturn;
    }

    /**
     * Allow parse an Object in String value
     * @param value
     * @return 
     */
    public static String getString(Object value) {
        return (value == null || value.toString().trim().equals("null")) ? "" : value.toString().trim();
    }
}
