package com.jjmsoftsolutions.Converter;

import com.jjmsoftsolutions.entity.Canton;
import com.jjmsoftsolutions.entity.Province;
import com.jjmsoftsolutions.model.CantonModel;
import com.jjmsoftsolutions.utils.StringUtils;
import static com.jjmsoftsolutions.utils.StringUtils.getInt;
import static java.lang.String.valueOf;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jonathan
 */
@FacesConverter("Canton")
public final class CantonConverter implements Converter {

    private List<Canton> list;

    public CantonConverter() {

    }

    public CantonConverter(Province province) {
        setList(province.getCantonList());
    }

    public void setList(List<Canton> list) {
        this.list = list;
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            int id = getInt(submittedValue);
            if (getList() != null) {
                for (Canton p : getList()) {
                    if (p.getIdCanton() == id) {
                        return p;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            String val = valueOf(((Canton) value).getIdCanton());
            return val;
        }
    }

    /**
     * @return the list
     */
    public List<Canton> getList() {
        return list;
    }
}
