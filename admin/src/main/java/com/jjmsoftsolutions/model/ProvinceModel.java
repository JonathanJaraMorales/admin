package com.jjmsoftsolutions.model;

import static com.jjmsoftsolutions.dataacces.DataAccess.findAll;
import com.jjmsoftsolutions.entity.Province;
import java.util.List;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class ProvinceModel {

    /**
     * Search the <strong>All</strong> province
     * @return 
     */
    public List<Province> getProvinceList() { 
        List<Province> list = (List) findAll(Province.class);
        return list;
    }
    
}
