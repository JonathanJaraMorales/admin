package com.jjmsoftsolutions.reports;

import com.jjmsoftsolutions.utils.Session;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class MasterReport<E extends Object> {

    private JasperPrint jasperPrint;
    private List<E> list;
    private String reportName;
   
    public MasterReport(List<E> list, String reportName) {
        this.list = list;
        this.reportName = reportName;
    }

    private void init(HashMap<String, Object> parameters) throws JRException {
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
        String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/Reports/" + reportName + ".jrxml");
        jasperPrint = JasperFillManager.fillReport(reportPath, parameters, beanCollectionDataSource);
    }
    
    public void addParameters(HashMap<String, Object> parameters){
        parameters.put("USER", Session.getUser().getUsername());
        parameters.put("CURRENT_DATE", new Date());
    }

    public void PDF(HashMap<String, Object> parameters){
        try {
            init(parameters);
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("Content-disposition", "attachment; filename=report.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (JRException | IOException ex) {
            Logger.getLogger(MasterReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
