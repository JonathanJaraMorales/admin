/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "LEADER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Leader.findAll", query = "SELECT l FROM Leader l"),
    @NamedQuery(name = "Leader.findByIdGroup", query = "SELECT l FROM Leader l WHERE l.leaderPK.idGroup = :idGroup"),
    @NamedQuery(name = "Leader.findByIdUser", query = "SELECT l FROM Leader l WHERE l.leaderPK.idUser = :idUser")})
public class Leader implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LeaderPK leaderPK;
    @JoinColumn(name = "ID_COMPANY", referencedColumnName = "ID_COMPANY")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company idCompany;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;
    @JoinColumn(name = "ID_GROUP", referencedColumnName = "ID_GROUP", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Groups groups;

    public Leader() {
    }

    public Leader(LeaderPK leaderPK) {
        this.leaderPK = leaderPK;
    }

    public Leader(int idGroup, int idUser) {
        this.leaderPK = new LeaderPK(idGroup, idUser);
    }

    public LeaderPK getLeaderPK() {
        return leaderPK;
    }

    public void setLeaderPK(LeaderPK leaderPK) {
        this.leaderPK = leaderPK;
    }

    public Company getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Company idCompany) {
        this.idCompany = idCompany;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Groups getGroups() {
        return groups;
    }

    public void setGroups(Groups groups) {
        this.groups = groups;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leaderPK != null ? leaderPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Leader)) {
            return false;
        }
        Leader other = (Leader) object;
        if ((this.leaderPK == null && other.leaderPK != null) || (this.leaderPK != null && !this.leaderPK.equals(other.leaderPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Leader[ leaderPK=" + leaderPK + " ]";
    }
    
}
