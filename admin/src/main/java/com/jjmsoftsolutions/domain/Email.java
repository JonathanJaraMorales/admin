package com.jjmsoftsolutions.domain;

import com.jjmsoftsolutions.utils.FileUtil;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import static javax.mail.Session.getDefaultInstance;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {

    private final Properties properties = new Properties();
    private Session session;

    private void init() {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.mail.sender", "masqjoven@gmail.com");
        properties.put("mail.smtp.password", "ccividaabundante");
        properties.put("mail.smtp.user", "masqjoven@gmail.com");
        properties.put("mail.smtp.auth", "true");
        session = getDefaultInstance(properties);
    }

    public boolean send(String emailTo, String subject, String message, String styleSheet) {
        init();
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender"), "+Q Joven"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
            msg.setSubject(subject);

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(overrideMessage(message, styleSheet), "text/html");

            MimeMultipart mp = new MimeMultipart();
            mp.addBodyPart(messageBodyPart);
            msg.setContent(mp);

            Transport t = session.getTransport("smtp");
            t.connect((String) properties.get("mail.smtp.user"), (String) properties.get("mail.smtp.password"));
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
            return true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean sendSms(String phoneNumber, String message) {
        initSms();
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender"), "+Q Joven"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(phoneNumber + "@sms.ice.cr"));
            msg.setSubject("+Q Joven Informa.");
            msg.setText(message);
            Transport t = session.getTransport("smtp");
            t.connect((String) properties.get("mail.smtp.user"), (String) properties.get("mail.smtp.password"));
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
            return true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    private void initSms() {
        properties.put("mail.smtp.host", "icecom.ice.co.cr");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.mail.sender", "pedrojara50000@ice.co.cr");
        properties.put("mail.smtp.password", "jara5000");
        properties.put("mail.smtp.port", "");
        properties.put("mail.smtp.user", "pedrojar@ice.co.cr");
        properties.put("mail.smtp.auth", "true");
        session = getDefaultInstance(properties);
    }

    public String overrideMessage(String message, String styleSheet) {
        String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/css/" + styleSheet + ".css");
        File f = new File(path);
        try {
            String value = new String(FileUtil.read(f));
            message = message.replace("</head>", "<style>" + value + "</style></head>");
        } catch (IOException ex) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, ex);
        }
        return message;
    }
}
