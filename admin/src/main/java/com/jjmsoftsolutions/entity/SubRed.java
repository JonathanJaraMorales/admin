/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "SUB_RED")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubRed.findAll", query = "SELECT s FROM SubRed s"),
    @NamedQuery(name = "SubRed.findByIdSubred", query = "SELECT s FROM SubRed s WHERE s.idSubred = :idSubred"),
    @NamedQuery(name = "SubRed.findByName", query = "SELECT s FROM SubRed s WHERE s.name = :name"),
    @NamedQuery(name = "SubRed.findByLeader1", query = "SELECT s FROM SubRed s WHERE s.leader1 = :leader1"),
    @NamedQuery(name = "SubRed.findByLeader2", query = "SELECT s FROM SubRed s WHERE s.leader2 = :leader2")})
public class SubRed implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SUBRED")
    private Integer idSubred;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "NAME")
    private String name;
    @Column(name = "LEADER1")
    private Integer leader1;
    @Column(name = "LEADER2")
    private Integer leader2;
    @JoinColumn(name = "ID_COMPANY", referencedColumnName = "ID_COMPANY")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company idCompany;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSubred", fetch = FetchType.LAZY)
    private List<User> userList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSubred", fetch = FetchType.LAZY)
    private List<Groups> groupsList;

    public SubRed() {
    }

    public SubRed(Integer idSubred) {
        this.idSubred = idSubred;
    }

    public SubRed(Integer idSubred, String name) {
        this.idSubred = idSubred;
        this.name = name;
    }

    public Integer getIdSubred() {
        return idSubred;
    }

    public void setIdSubred(Integer idSubred) {
        this.idSubred = idSubred;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLeader1() {
        return leader1;
    }

    public void setLeader1(Integer leader1) {
        this.leader1 = leader1;
    }

    public Integer getLeader2() {
        return leader2;
    }

    public void setLeader2(Integer leader2) {
        this.leader2 = leader2;
    }

    public Company getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Company idCompany) {
        this.idCompany = idCompany;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<Groups> getGroupsList() {
        return groupsList;
    }

    public void setGroupsList(List<Groups> groupsList) {
        this.groupsList = groupsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubred != null ? idSubred.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubRed)) {
            return false;
        }
        SubRed other = (SubRed) object;
        if ((this.idSubred == null && other.idSubred != null) || (this.idSubred != null && !this.idSubred.equals(other.idSubred))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.SubRed[ idSubred=" + idSubred + " ]";
    }
    
}
