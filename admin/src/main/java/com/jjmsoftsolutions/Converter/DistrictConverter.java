/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.Converter;

import com.jjmsoftsolutions.entity.Canton;
import com.jjmsoftsolutions.entity.District;
import com.jjmsoftsolutions.model.DistrictModel;
import com.jjmsoftsolutions.utils.StringUtils;
import static com.jjmsoftsolutions.utils.StringUtils.getInt;
import static java.lang.String.valueOf;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jonathan
 */
@FacesConverter("District")
public final class DistrictConverter implements Converter {

    private List<District> list;

    public DistrictConverter() {

    }

    public DistrictConverter(Canton canton) {
        setList(canton.getDistrictList());
    }

    public void setList(List<District> list) {
        this.list = list;
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            int id = getInt(submittedValue);
            for (District p : getList()) {
                if (p.getIdDistrict() == id) {
                    return p;
                }
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            String val = valueOf(((District) value).getIdDistrict());
            return val;
        }
    }

    /**
     * @return the list
     */
    public List<District> getList() {
        return list;
    }

}
