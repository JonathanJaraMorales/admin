package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.reports.MensualReport;
import com.jjmsoftsolutions.utils.StringUtils;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "mensualReport")
@ViewScoped
public class MensualReportBean {
    
    private String selectedMonth;
    private int year;
    private Groups selectedGroup = null;
    private List<Groups> groups = null;

    public void generateReport(){
        MensualReport report = new MensualReport();
        report.printPDF(StringUtils.getInt(selectedMonth), year);
    }
    /**
     * @return the selectedMonth
     */
    public String getSelectedMonth() {
        return selectedMonth;
    }

    /**
     * @param selectedMonth the selectedMonth to set
     */
    public void setSelectedMonth(String selectedMonth) {
        this.selectedMonth = selectedMonth;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the selectedGroup
     */
    public Groups getSelectedGroup() {
        return selectedGroup;
    }

    /**
     * @param selectedGroup the selectedGroup to set
     */
    public void setSelectedGroup(Groups selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    /**
     * @return the groups
     */
    public List<Groups> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }
    
}
