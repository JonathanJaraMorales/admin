/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "CONSOLIDATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consolidation.findAll", query = "SELECT c FROM Consolidation c"),
    @NamedQuery(name = "Consolidation.findByIdUser", query = "SELECT c FROM Consolidation c WHERE c.consolidationPK.idUser = :idUser"),
    @NamedQuery(name = "Consolidation.findByIdGroup", query = "SELECT c FROM Consolidation c WHERE c.consolidationPK.idGroup = :idGroup"),
    @NamedQuery(name = "Consolidation.findBySendDate", query = "SELECT c FROM Consolidation c WHERE c.sendDate = :sendDate"),
    @NamedQuery(name = "Consolidation.findByResponseDate", query = "SELECT c FROM Consolidation c WHERE c.responseDate = :responseDate"),
    @NamedQuery(name = "Consolidation.findByComments", query = "SELECT c FROM Consolidation c WHERE c.comments = :comments")})
public class Consolidation implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConsolidationPK consolidationPK;
    @Column(name = "SEND_DATE")
    @Temporal(TemporalType.DATE)
    private Date sendDate;
    @Column(name = "RESPONSE_DATE")
    @Temporal(TemporalType.DATE)
    private Date responseDate;
    @Size(max = 1000)
    @Column(name = "COMMENTS")
    private String comments;
    @JoinColumn(name = "ID_COMPANY", referencedColumnName = "ID_COMPANY")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company idCompany;
    @JoinColumn(name = "ID_GROUP", referencedColumnName = "ID_GROUP", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Groups groups;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    public Consolidation() {
    }

    public Consolidation(ConsolidationPK consolidationPK) {
        this.consolidationPK = consolidationPK;
    }

    public Consolidation(int idUser, int idGroup) {
        this.consolidationPK = new ConsolidationPK(idUser, idGroup);
    }

    public ConsolidationPK getConsolidationPK() {
        return consolidationPK;
    }

    public void setConsolidationPK(ConsolidationPK consolidationPK) {
        this.consolidationPK = consolidationPK;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Company getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Company idCompany) {
        this.idCompany = idCompany;
    }

    public Groups getGroups() {
        return groups;
    }

    public void setGroups(Groups groups) {
        this.groups = groups;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consolidationPK != null ? consolidationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consolidation)) {
            return false;
        }
        Consolidation other = (Consolidation) object;
        if ((this.consolidationPK == null && other.consolidationPK != null) || (this.consolidationPK != null && !this.consolidationPK.equals(other.consolidationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Consolidation[ consolidationPK=" + consolidationPK + " ]";
    }
    
}
