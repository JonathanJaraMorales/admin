package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.entity.Canton;
import com.jjmsoftsolutions.entity.Province;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "cantonController")
@ViewScoped
public class CantonController implements Serializable {

    private Province province;

    private Canton selectedCanton;
    private List<Canton> cantons;

    private DistrictController districtController;

    public CantonController() {
        districtController = new DistrictController();
    }

    /**
     * @return the selectedCanton
     */
    public Canton getSelectedCanton() {
        return selectedCanton;
    }

    /**
     * @param selectedCanton the selectedCanton to set
     */
    public void setSelectedCanton(Canton selectedCanton) {
        this.selectedCanton = selectedCanton;
    }

    /**
     * @return the cantons
     */
    public List<Canton> getCantons() {
        if (getProvince() != null) {
            setCantons(getProvince().getCantonList());
            return cantons;
        } else {
            return null;
        }
    }

    /**
     * @param cantons the cantons to set
     */
    public void setCantons(List<Canton> cantons) {
        this.cantons = cantons;
    }

    /**
     * @return the districtController
     */
    public DistrictController getDistrictController() {
        if(selectedCanton != null){
            districtController.setCanton(selectedCanton);
        }
        return districtController;
    }

    /**
     * @param districtController the districtController to set
     */
    public void setDistrictController(DistrictController districtController) {
        this.districtController = districtController;
    }

    /**
     * @return the province
     */
    public Province getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(Province province) {
        this.province = province;
    }

}
