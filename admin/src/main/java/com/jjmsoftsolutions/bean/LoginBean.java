package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.entity.User;
import com.jjmsoftsolutions.model.LoginModel;
import com.jjmsoftsolutions.utils.Context;
import static com.jjmsoftsolutions.utils.Context.showMessage;
import com.jjmsoftsolutions.utils.Session;
import static com.jjmsoftsolutions.utils.Session.getSession;
import static com.jjmsoftsolutions.utils.Session.redirect;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

@ManagedBean(name = "login")
@SessionScoped
public class LoginBean {

    private String name;
    private String password;
    private User user;

    public void login() {
        LoginModel model = new LoginModel();
        user = model.login(name, password);
        boolean result = user != null;
        if (result) {
            HttpSession session = getSession();
            session.setAttribute("user", user);
            redirect();
        }

        showMessage("Bienvenid@", "Credenciales no válidas", result);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogin() {
        return getUser() == null;
    }

    public void logout() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        setUser(null);
        FacesContext facesContext = getCurrentInstance();
        String redirect = "login.xhtml";
        NavigationHandler myNav = facesContext.getApplication().getNavigationHandler();
        myNav.handleNavigation(facesContext, null, redirect);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
