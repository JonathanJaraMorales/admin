/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.entity.Consolidation;
import com.jjmsoftsolutions.entity.ConsolidationPK;
import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.entity.User;
import com.jjmsoftsolutions.model.ConsolidationModel;
import com.jjmsoftsolutions.utils.Session;
import java.util.Date;

/**
 *
 * @author jonathan
 */
public class ConsolidationController {
    
    public static boolean insert(String comments, Groups group, User user, Date date){
        Consolidation consolidation = new Consolidation();
        consolidation.setComments(comments);
        consolidation.setConsolidationPK(new ConsolidationPK(user.getIdUser(), group.getIdGroup()));
        consolidation.setSendDate(date);
        consolidation.setIdCompany(Session.getCompany());
        return ConsolidationModel.insert(consolidation);
    }
    
}
