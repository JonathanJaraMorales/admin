/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "GROUPS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Groups.findAll", query = "SELECT g FROM Groups g"),
    @NamedQuery(name = "Groups.findByIdGroup", query = "SELECT g FROM Groups g WHERE g.idGroup = :idGroup"),
    @NamedQuery(name = "Groups.findByAddress", query = "SELECT g FROM Groups g WHERE g.address = :address"),
    @NamedQuery(name = "Groups.findByOpenDate", query = "SELECT g FROM Groups g WHERE g.openDate = :openDate"),
    @NamedQuery(name = "Groups.findByDay", query = "SELECT g FROM Groups g WHERE g.day = :day"),
    @NamedQuery(name = "Groups.findByHour", query = "SELECT g FROM Groups g WHERE g.hour = :hour")})
public class Groups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_GROUP")
    private Integer idGroup;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "ADDRESS")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPEN_DATE")
    @Temporal(TemporalType.DATE)
    private Date openDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DAY")
    private char day;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HOUR")
    @Temporal(TemporalType.TIME)
    private Date hour;
    @OneToMany(mappedBy = "idGroup", fetch = FetchType.LAZY)
    private List<User> userList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGroup", fetch = FetchType.LAZY)
    private List<Message> messageList;
    @JoinColumn(name = "ID_SUBRED", referencedColumnName = "ID_SUBRED")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SubRed idSubred;
    @JoinColumn(name = "ID_COMPANY", referencedColumnName = "ID_COMPANY")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company idCompany;
    @JoinColumn(name = "ID_DISTRICT", referencedColumnName = "ID_DISTRICT")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private District idDistrict;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groups", fetch = FetchType.LAZY)
    private List<Leader> leaderList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groups", fetch = FetchType.LAZY)
    private List<Consolidation> consolidationList;

    public Groups() {
    }

    public Groups(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public Groups(Integer idGroup, String address, Date openDate, char day, Date hour) {
        this.idGroup = idGroup;
        this.address = address;
        this.openDate = openDate;
        this.day = day;
        this.hour = hour;
    }

    public Integer getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public char getDay() {
        return day;
    }

    public void setDay(char day) {
        this.day = day;
    }

    public Date getHour() {
        return hour;
    }

    public void setHour(Date hour) {
        this.hour = hour;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }

    public SubRed getIdSubred() {
        return idSubred;
    }

    public void setIdSubred(SubRed idSubred) {
        this.idSubred = idSubred;
    }

    public Company getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Company idCompany) {
        this.idCompany = idCompany;
    }

    public District getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(District idDistrict) {
        this.idDistrict = idDistrict;
    }

    @XmlTransient
    public List<Leader> getLeaderList() {
        return leaderList;
    }

    public void setLeaderList(List<Leader> leaderList) {
        this.leaderList = leaderList;
    }

    @XmlTransient
    public List<Consolidation> getConsolidationList() {
        return consolidationList;
    }

    public void setConsolidationList(List<Consolidation> consolidationList) {
        this.consolidationList = consolidationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGroup != null ? idGroup.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groups)) {
            return false;
        }
        Groups other = (Groups) object;
        if ((this.idGroup == null && other.idGroup != null) || (this.idGroup != null && !this.idGroup.equals(other.idGroup))) {
            return false;
        }
        return true;
    }

    public String getCompleteDay() {
        if (day == 'L') {
            return "Lunes";
        } else if (day == 'K') {
            return "Martes";
        } else if (day == 'M') {
            return "Miércoles";
        } else if (day == 'J') {
            return "Jueves";
        } else if (day == 'V') {
            return "Viernes";
        } else if (day == 'S') {
            return "Sábado";
        } else if (day == 'D') {
            return "Domingo";
        } else {
            return "";
        }
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Groups[ idGroup=" + idGroup + " ]";
    }

}
