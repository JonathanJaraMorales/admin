package com.jjmsoftsolutions.Converter;

import com.jjmsoftsolutions.entity.Groups;
import com.jjmsoftsolutions.utils.StringUtils;
import static com.jjmsoftsolutions.utils.StringUtils.getInt;
import static java.lang.String.valueOf;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("Groups")
@ViewScoped
public class GroupConverter implements Converter {
    
    private static List<Groups> list;
    
    public GroupConverter(){
        
    }
    
    public GroupConverter(List<Groups> list){
        setList(list);
    }
    
    public void setList(List<Groups> list){
        this.list = list;
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int id = getInt(submittedValue);
                for (Groups p : getList()) {
                    if (p.getIdGroup()== id) {
                        return p;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid player"));
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            String val =  valueOf(((Groups) value).getIdGroup());
            return val;
        }
    }

    /**
     * @return the list
     */
    public List<Groups> getList() {
        return list;
    }
    
}
