package com.jjmsoftsolutions.Converter;

import com.jjmsoftsolutions.entity.Province;
import com.jjmsoftsolutions.model.ProvinceModel;
import com.jjmsoftsolutions.utils.StringUtils;
import static com.jjmsoftsolutions.utils.StringUtils.getInt;
import static java.lang.String.valueOf;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jonathan
 */
@FacesConverter("Province")
public final class ProvinceConverter implements Converter {
    
    private List<Province> list;
    
    public ProvinceConverter(){
        setList(new ProvinceModel().getProvinceList());
    }
    
    public void setList(List<Province> list){
        this.list = list;
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int id = getInt(submittedValue);
                for (Province p : getList()) {
                    if (p.getIdProvince() == id) {
                        return p;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid player"));
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            String val =  valueOf(((Province) value).getIdProvince());
            return val;
        }
    }

    /**
     * @return the list
     */
    public List<Province> getList() {
        return list;
    }

}
