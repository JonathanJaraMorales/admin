package com.jjmsoftsolutions.dataacces;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import static javax.persistence.Persistence.createEntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

public class DataAccess {

    private static  EntityManagerFactory factory = createEntityManagerFactory("Admin");
    
    private DataAccess() {
    }

    private static EntityManager getEntityManager() {
       
        EntityManager manager = factory.createEntityManager();
        return manager;
    }

    public static <T extends Object> List<Object> findAll(Class clazz) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM ").append(clazz.getSimpleName()).append(" U ");
        TypedQuery query = getEntityManager().createQuery(sql.toString(), clazz);
        return query.getResultList();
    }

    public static <T extends Object> List<Object> findAll(Class clazz, String sql, Map parameters) {
        TypedQuery query = getEntityManager().createQuery(sql.toString(), clazz);
        Iterator iterator = parameters.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry e = (Map.Entry) iterator.next();
            query.setParameter((String) e.getKey(), e.getValue());
        }
        return query.getResultList();
    }

    public static <T extends Object> List<Object> findAllByDate(Class clazz, String sql, Map parameters) {
        TypedQuery query = getEntityManager().createQuery(sql.toString(), clazz);
        Iterator iterator = parameters.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry e = (Map.Entry) iterator.next();
            query.setParameter((String) e.getKey(), (Date) e.getValue(), TemporalType.DATE);
        }
        return query.getResultList();
    }

    public static <T extends Object> List<Object> findAll(Class clazz, String sql) {
        TypedQuery query = getEntityManager().createQuery(sql.toString(), clazz);
        return query.getResultList();
    }

    public static <T extends Object> Object find(Class pclazz, Integer id) {
        return getEntityManager().find(pclazz, id);
    }

    public static <T extends Object> Object find(Class pclazz, String sql) {
        return find(pclazz, sql, null);
    }

    public static <T extends Object> Object findTop(Class pclazz, String sql) {
        try {
            TypedQuery query = getEntityManager().createQuery(sql, pclazz);
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (PersistenceException ex2) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    public static <T extends Object> Object findTop(Class pclazz, String sql, HashMap parameters) {
        try {
            TypedQuery query = getEntityManager().createQuery(sql, pclazz);
            Iterator iterator = parameters.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry e = (Map.Entry) iterator.next();
                query.setParameter((String) e.getKey(), (String) e.getValue());
            }
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (PersistenceException ex2) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    public static <T extends Object> List<Object> findAll(Class pclazz, String sql, int totalRows) {
        try {
            TypedQuery query = getEntityManager().createQuery(sql, pclazz);
            query.setMaxResults(totalRows);
            return query.getResultList();
        } catch (PersistenceException ex2) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    public static <T extends Object> List<Object> findAll(Class pclazz, String sql, Map parameters, int totalRows) {
        try {
            TypedQuery query = getEntityManager().createQuery(sql, pclazz);
            Iterator iterator = parameters.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry e = (Map.Entry) iterator.next();
                query.setParameter((String) e.getKey(), e.getValue());
            }
            query.setMaxResults(totalRows);
            return query.getResultList();
        } catch (PersistenceException ex2) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    public static <T extends Object> Object find(Class pclazz, String sql, Map parameters) {
        try {
            TypedQuery query = getEntityManager().createQuery(sql, pclazz);
            Iterator iterator = parameters.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry e = (Map.Entry) iterator.next();
                query.setParameter((String) e.getKey(), e.getValue());
            }

            return query.getSingleResult();
        } catch (PersistenceException ex2) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean insert(Object pclazz) {

        EntityTransaction transaction = null;
        EntityManager manager = null;

        try {
            manager = getEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.merge(pclazz);
            //manager.persist(pclazz);
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace(System.out);
            return false;

        } finally {
            manager.close();
        }
    }

    public static <T extends Object> Object doInsert(Object pclazz) {

        EntityTransaction transaction = null;
        EntityManager manager = null;

        try {
            manager = getEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            pclazz = manager.merge(pclazz);
            //manager.persist(pclazz);
            transaction.commit();
            return pclazz;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace(System.out);
            return null;

        } finally {
            manager.close();
        }
    }

    public static boolean insert(List<Object> list) {

        EntityTransaction transaction = null;
        EntityManager manager = null;

        try {
            manager = getEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            for (Object object : list) {
                manager.persist(object);
            }
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace(System.out);
            return false;

        } finally {
            manager.close();
        }
    }

    public static boolean update(String sql) {

        EntityTransaction transaction = null;
        EntityManager manager = null;

        try {
            manager = getEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery(sql);
            query.executeUpdate();
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace(System.out);
            return false;

        } finally {
            manager.close();
        }
    }

    public static boolean update(Object pclazz) {

        EntityTransaction transaction = null;
        EntityManager manager = null;

        try {
            manager = getEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.merge(pclazz);
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace(System.out);
            return false;

        } finally {
            manager.close();
        }

    }

    public static boolean delete(Class clazz, Integer id) {
        EntityTransaction transaction = null;
        EntityManager manager = null;

        try {
            manager = getEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Object object = manager.find(clazz, id);
            manager.remove(object);
            //manager.flush();
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace(System.out);
            return false;

        } finally {
            manager.close();
        }
    }


}
