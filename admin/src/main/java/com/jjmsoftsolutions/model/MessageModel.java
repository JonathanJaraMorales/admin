package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import static com.jjmsoftsolutions.dataacces.DataAccess.findAllByDate;
import com.jjmsoftsolutions.entity.Message;
import com.jjmsoftsolutions.entity.UserMessage;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MessageModel {

    public static Message insert(Message message) {
        return (Message) DataAccess.doInsert(message);
    }

    public static boolean insertMessageUser(UserMessage usrMessage) {
        return DataAccess.insert(usrMessage);
    }

    public static List<Message> getMessagesByMonth(Date toDate, Date fromDate) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT M From Message M ");
        sql.append("WHERE M.messageDate BETWEEN :toDate '' AND :fromDate '' ");        
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("toDate", toDate);
        parameters.put("fromDate", fromDate);
        List<Message> list = (List) findAllByDate(Message.class, sql.toString(), parameters);
        return list;
    }

}
