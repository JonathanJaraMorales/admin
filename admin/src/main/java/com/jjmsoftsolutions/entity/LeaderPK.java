/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luis
 */
@Embeddable
public class LeaderPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_GROUP")
    private int idGroup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USER")
    private int idUser;

    public LeaderPK() {
    }

    public LeaderPK(int idGroup, int idUser) {
        this.idGroup = idGroup;
        this.idUser = idUser;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idGroup;
        hash += (int) idUser;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaderPK)) {
            return false;
        }
        LeaderPK other = (LeaderPK) object;
        if (this.idGroup != other.idGroup) {
            return false;
        }
        if (this.idUser != other.idUser) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.LeaderPK[ idGroup=" + idGroup + ", idUser=" + idUser + " ]";
    }
    
}
