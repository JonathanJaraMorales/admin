package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.entity.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserController implements Serializable {

    public static List<User> getUsersByGroupAndFirstName(List<User> userList, String firstName) {
        List<User> list = new ArrayList<User>();
        for (User user : userList) {
            if (user.getFirstName().toUpperCase().contains(firstName.toUpperCase())) {
                list.add(user);
            }
        }
        return list;
    }
}
