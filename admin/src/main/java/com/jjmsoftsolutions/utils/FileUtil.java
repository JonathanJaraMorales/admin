/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class FileUtil {
    
        public final static int BUF_SIZE = 1024;
    
     public static byte[] read(File pFile) throws IOException {

        // Custom implementation, as we know the size of a file

        if (!pFile.exists()) {

            throw new FileNotFoundException(pFile.toString());

        }



        byte[] bytes = new byte[(int) pFile.length()];

        InputStream in = null;



        try {

            // Use buffer size two times byte array, to avoid i/o bottleneck

            in = new BufferedInputStream(new FileInputStream(pFile), BUF_SIZE * 2);



            int off = 0;

            int len;

            while ((len = in.read(bytes, off, in.available())) != -1 && (off < bytes.length)) {

                off += len;

                //              System.out.println("read:" + len);

            }

        }

        // Just pass any IOException on up the stack

        finally {

            close(in);

        }



        return bytes;

    }

     public static void close(InputStream pInput) {

        try {

            if (pInput != null) {

                pInput.close();

            }

        }

        catch (IOException ignore) {

            // Non critical error

        }

    }
}
