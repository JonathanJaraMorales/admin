package com.jjmsoftsolutions.dataacces;

public final class TransactionElement {
    
    private TransactionType type;
    private Object transactionObject;
    private Class clazz;
    
    public TransactionElement(TransactionType type, Object transactionObject, Class clazz){
        setType(type);
        setTransactionObject(transactionObject);
        setClazz(clazz);
    }

    /**
     * @return the type
     */
    public TransactionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(TransactionType type) {
        this.type = type;
    }

    /**
     * @return the transactionObject
     */
    public Object getTransactionObject() {
        return transactionObject;
    }

    /**
     * @param transactionObject the transactionObject to set
     */
    public void setTransactionObject(Object transactionObject) {
        this.transactionObject = transactionObject;
    }

    /**
     * @return the clazz
     */
    public Class getClazz() {
        return clazz;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
    
    
    
}
